# The new atomic version of centos
FROM registry.centos.org/kbsingh/centos-atomic:7-alpha2

# Install nc
RUN microdnf install nc;\
 microdnf clean all

# Switch to non root user
USER 1001

# Run a simple websever on port 8080 with netcat: http://stackoverflow.com/a/19139134
EXPOSE 8080
CMD while true ; do /usr/bin/nc -l -p 8080 -c 'echo -e "HTTP/1.1 200 OK\n\n Hello world!\n Server time: $(date)"'; done