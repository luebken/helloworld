# helloworld

A simple helloworld showcasing atomic base images


# Run latest image

```
$ docker run -P luebken/helloworld
```

# Local build
```
# build
docker build -t luebken/helloworld .

# push image to docker hub
docker push luebken/helloworld
```


# OpenShift

## Local: Start minishift

```
# install https://github.com/minishift/minishift
minishift start
```

## openshift online: Login
```
# get free account on openshift.com
# get token in webconsole (> commandline)
oc login ...
``` 

## Run on openshift {local | online}
```
kubectl create -f k8s/deployment.yaml
kubectl create -f k8s/service.yaml
oc create -f k8s/route.yaml
oc get route -o json | jq .items[0].spec.host
```

# Gitlab pipeline

* Create project on Gitlab
* Add git remote. E.g.: `git remote add gitlab git@gitlab.com:luebken/helloworld.git`
* set $DOCKER_HUB_USER and $DOCKER_HUB_PASSWORD in Project > Settings > CI/CD Pipelines
* push a change